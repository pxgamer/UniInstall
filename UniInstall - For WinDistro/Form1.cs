﻿using System;
using System.Windows.Forms;
using System.Collections;

namespace UniInstall___For_WinDistro
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string folder = Loc.folder;
            ArrayList myArray = new ArrayList();
            foreach (object checkedItem in checkedListBox1.CheckedItems)
            {
                // Add to the array
                myArray.Add(checkedItem.ToString());
            }

            System.IO.Directory.CreateDirectory(folder + "/Serials");

            for (int i = 0; i < myArray.Count; i++)
            {
                string soft = myArray[i].ToString();
                if (soft == "Atom")
                {
                    inWork.Text = Loc.path + Loc.atom;
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.atom);
                    proc.WaitForExit();
                }
                else if (soft == "Firefox")
                {
                    inWork.Text = Loc.path + Loc.firefox;
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.firefox);
                    proc.WaitForExit();
                }
                else if (soft == "SMPlayer")
                {
                    inWork.Text = Loc.path + Loc.smplayer;
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.smplayer);
                    proc.WaitForExit();
                }
                else if (soft == "Lightshot")
                {
                    inWork.Text = Loc.path + Loc.lightshot;
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.lightshot);
                    proc.WaitForExit();
                }
                else if (soft == "MalwareBytes Anti-Exploit")
                {
                    inWork.Text = Loc.path + Loc.mbae;
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.mbae);
                    proc.WaitForExit();
                    System.IO.File.Copy(Loc.path + @"\MalwareBytes Anti-Exploit\Serial.txt", folder + "/Serials/MBAE Serial.txt");
                }
                else if (soft == "MalwareBytes Anti-Malware")
                {
                    inWork.Text = Loc.path + Loc.mbam;
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.mbam);
                    proc.WaitForExit();
                    System.IO.File.Copy(Loc.path + @"\MalwareBytes Anti-Malware\Serial.txt", folder + "/Serials/MBAM Serial.txt");
                }
                else if (soft == "Photoshop CS6 Extended")
                {
                    inWork.Text = Loc.path + Loc.photoshop;
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.photoshop);
                    proc.WaitForExit();
                    System.IO.File.Copy(Loc.path + @"\D:\Backup Software\Photoshop CS6 Extended\DLL FILE\64bit\amtlib.dll", @"C:\Program Files\Adobe\Adobe Photoshop CS6 (64 Bit)\amtlib.dll");
                }
                else if (soft == "Private Internet Access")
                {
                    inWork.Text = Loc.path + Loc.pia;
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.pia);
                    proc.WaitForExit();
                    System.IO.File.Copy(Loc.path + @"\Private Internet Access\Serial.txt", folder + "/Serials/PIA Serial.txt");
                }
                else if (soft == "Sandboxie")
                {
                    inWork.Text = Loc.path + Loc.sandboxie;
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.sandboxie);
                    proc.WaitForExit();
                    proc = System.Diagnostics.Process.Start(Loc.path + @"\Sandboxie\Crack\Step 1");
                    proc.WaitForExit();
                    proc = System.Diagnostics.Process.Start(Loc.path + @"\Sandboxie\Crack\Step 2");
                    proc.WaitForExit();
                }
                else if (soft == "Shadow Defender")
                {
                    inWork.Text = Loc.path + Loc.shadow_defender;
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.shadow_defender);
                    proc.WaitForExit();
                    System.IO.File.Copy(Loc.path + @"\Shadow Defender\Serial.txt", folder + "/Serials/Shadow Defender Serial.txt");
                }
                else if (soft == "Skype")
                {
                    inWork.Text = Loc.path + Loc.skype;
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.skype);
                    proc.WaitForExit();
                }
                else if (soft == "Visual Studio 2013 Pro")
                {
                    inWork.Text = Loc.path + Loc.vs2013pro;

                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.vs2013pro);
                    proc.WaitForExit();
                    System.IO.File.Copy(Loc.path + @"\Visual Studio 2013 Pro\Serial.txt", folder + "/Serials/VS2013Pro Serial.txt");
                }
                else if (soft == "Visual Studio 2015 Community (Non-Default)")
                {
                    inWork.Text = Loc.path + Loc.vs2015com;
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.vs2015com);
                    proc.WaitForExit();
                }
                else if (soft == "WinRAR")
                {
                    inWork.Text = Loc.path + Loc.winrar;
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.winrar);
                    proc.WaitForExit();
                    System.IO.File.Copy(Loc.path + @"\WinRAR\rarreg.key", @"C:\Program Files\WinRAR\rarreg.key");
                }
                else if (soft == "WiseCare 365")
                {
                    inWork.Text = Loc.path + Loc.wisecare365;
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.wisecare365);
                    proc.WaitForExit();
                    System.IO.File.Copy(Loc.path + @"\WiseCare 365\Serial.txt", folder + "/Serials/WiseCare 365 Serial.txt");
                }
                else if (soft == "Wise Folder Hider Pro")
                {
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.wisefhpro);
                    proc.WaitForExit();
                }
                else if (soft == "Microsoft Office 2013")
                {
                    inWork.Text = Loc.path + Loc.msoffice2013;
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.msoffice2013);
                    proc.WaitForExit();
                    System.IO.File.Copy(Loc.path + @"\Microsoft Office 2013\Serial.txt", folder + "/Serials/Microsoft Office 2013 Serial.txt");
                }
                else if (soft == "Winamp")
                {
                    inWork.Text = Loc.path + Loc.winamp;
                    var proc = System.Diagnostics.Process.Start(Loc.path + Loc.winamp);
                    proc.WaitForExit();
                    System.IO.File.Copy(Loc.path + @"\Winamp\Serial.txt", folder + "/Serials/Winamp Serial.txt");
                }

            }
            End ed = new End();
            ed.Show();
            this.Hide();
        }
    }
}
