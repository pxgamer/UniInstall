﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace UniInstall___For_WinDistro
{
    class Loc
    {
        public static string path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

        public static string atom = @"/Atom/Atom.exe";
        public static string firefox = @"/Firefox/Firefox.exe";
        public static string smplayer = @"/SMPlayer/SMPlayer.exe";
        public static string lightshot = @"/Lightshot/Lightshot.exe";
        public static string mbae = @"/MalwareBytes Anti-Exploit/MalwareBytes Anti-Exploit.exe";
        public static string mbam = @"/MalwareBytes Anti-Malware/MalwareBytes Anti-Malware.exe";
        public static string photoshop = @"/Photoshop CS6 Extended/SOFTWARE/Set-up.exe";
        public static string pia = @"/Private Internet Access/Private Internet Access.exe";
        public static string sandboxie = @"/Sandboxie/Sandboxie.exe";
        public static string shadow_defender = @"/Shadow Defender/Shadow Defender.exe";
        public static string skype = @"/Skype/Skype.exe";
        public static string vs2013pro = @"/Visual Studio 2013 Pro/vs_professional.exe";
        public static string vs2015com = @"/Visual Studio 2015 Community/Visual Studio 2015 Community.exe";
        public static string winrar = @"/WinRAR/WinRAR.exe";
        public static string wisecare365 = @"/WiseCare 365/WiseCare 365.exe";
        public static string wisefhpro = @"/Wise Folder Hider Pro/Wise Folder Hider Pro.exe";
        public static string msoffice2013 = @"/Microsoft Office 2013/Microsoft Office 2013.exe";
        public static string winamp = @"/Winamp/Winamp.exe";
        public static string folder = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
    }
}
